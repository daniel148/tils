import numpy as np
import cv2 as cv
import Otsu as otsu
import GaussianMixtureModel as gmm
from matplotlib import pyplot as plt
from scipy.stats import norm
import GMMWrapper as GMMWrapper

# Read image
img = cv.imread('D:\TILS\DANE _HE\WEZEL_CHLONNY_2.tif',1)
blue_channel = img[:,:,0].flatten()
green_channel = img[:,:,1].flatten()
red_channel = img[:,:,2].flatten()

#Otsu Method
blue_bins, blue_mask = otsu.get_mask_for_channel(blue_channel)
green_bins, green_mask = otsu.get_mask_for_channel(green_channel)
red_bins, red_mask = otsu.get_mask_for_channel(red_channel)
mask = np.where(np.sum([blue_mask, green_mask, red_mask], axis=0) == 3, 1, 0)
blue = blue_channel[mask == 1]
green = green_channel[mask == 1]
red = red_channel[mask == 1]

plt.subplot(3, 3, 1)
blue_counts, _, _ = plt.hist(blue, facecolor='blue', bins=blue_bins)
plt.title('Histograms')
plt.ylabel('Blue channel')
plt.subplot(3, 3, 4)
green_counts, _, _ = plt.hist(green, facecolor='green', bins=green_bins)
plt.ylabel('Green channel')
plt.subplot(3, 3, 7)
red_counts, _, _ = plt.hist(red, facecolor='red', bins=red_bins)
plt.ylabel('Red channel')

blue_bins = list(range(0, blue_bins))
green_bins = list(range(0, green_bins))
red_bins = list(range(0, red_bins))
#GMM
blue_gaussian_mixture_model = GMMWrapper.decomposeGMM(blue_bins, blue_counts, 0.0166, 10)
green_gaussian_mixture_model = GMMWrapper.decomposeGMM(green_bins, green_counts, 0.0166, 10)
red_gaussian_mixture_model = GMMWrapper.decomposeGMM(red_bins, red_counts, 0.0166, 10)

blue_gaussian_decomposition = []
plt.subplot(3, 3, 2)
for gaussian_component in blue_gaussian_mixture_model:
    x_axis = np.arange(0, 256, 0.001)
    blue_gaussian_decomposition.append(gaussian_component.weight*norm.pdf(x_axis, gaussian_component.mean,
                              gaussian_component.deviation))
    plt.plot(x_axis, gaussian_component.weight*norm.pdf(x_axis, gaussian_component.mean,
                              gaussian_component.deviation))
    plt.title('Decomposition')
    
plt.subplot(3, 3, 1)
blue_gaussian_sum = [sum(i) for i in zip(*blue_gaussian_decomposition)]
plt.plot(x_axis, blue_gaussian_sum)
plt.title('Sum of Gaussian components')


green_gaussian_decomposition = []
plt.subplot(3, 3, 5)
for gaussian_component in green_gaussian_mixture_model:
    x_axis = np.arange(0, 256, 0.001)
    green_gaussian_decomposition.append(gaussian_component.weight*norm.pdf(x_axis, gaussian_component.mean,
                              gaussian_component.deviation))
    plt.plot(x_axis, gaussian_component.weight*norm.pdf(x_axis, gaussian_component.mean,
                              gaussian_component.deviation))
    
plt.subplot(3, 3, 4)
green_gaussian_sum = [sum(i) for i in zip(*green_gaussian_decomposition)]
plt.plot(x_axis, green_gaussian_sum)

red_gaussian_decomposition = []
plt.subplot(3, 3, 8)
for gaussian_component in red_gaussian_mixture_model:
    x_axis = np.arange(0, 256, 0.001)
    red_gaussian_decomposition.append(gaussian_component.weight*norm.pdf(x_axis, gaussian_component.mean,
                              gaussian_component.deviation))
    plt.plot(x_axis, gaussian_component.weight*norm.pdf(x_axis, gaussian_component.mean,
                             gaussian_component.deviation))
    
plt.subplot(3, 3, 7)
red_gaussian_sum = [sum(i) for i in zip(*red_gaussian_decomposition)]
plt.plot(x_axis, red_gaussian_sum)

plt.subplot(3, 3, 3)
for gaussian_component in blue_gaussian_decomposition:
    x_axis = np.arange(0, 256, 0.001)
    plt.plot(x_axis, gaussian_component/blue_gaussian_sum)
    plt.title('Feature definition')

plt.subplot(3, 3, 6)
for gaussian_component in green_gaussian_decomposition:
    x_axis = np.arange(0, 256, 0.001)
    plt.plot(x_axis, gaussian_component/green_gaussian_sum)
    
plt.subplot(3, 3, 9)
for gaussian_component in red_gaussian_decomposition:
    x_axis = np.arange(0, 256, 0.001)
    plt.plot(x_axis, gaussian_component/red_gaussian_sum)

