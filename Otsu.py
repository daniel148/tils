import numpy as np
import GMMWrapper as GMMWrapper
from scipy.stats import norm

def get_mask_for_channel(channel):
    bins = list(range(0, 256))
    counts, _ = np.histogram(channel, bins=256)
    
    #GMM
    gaussian_mixture_model = GMMWrapper.decomposeGMM(bins, counts, 0.0166, 10)
    
    gaussian_decomposition = []
    gaussian_mixture_model.sort(key=lambda x: (x.weight*norm(x.mean, x.deviation).pdf(x.mean)), reverse=False)
    for gaussian_component in gaussian_mixture_model:
        x_axis = np.arange(0, 256)
        gaussian_decomposition.append(gaussian_component.weight*norm.pdf(x_axis, gaussian_component.mean,
                              gaussian_component.deviation))
        
    threshold = 0
    highest_gauss = gaussian_decomposition[-1]
    for gauss in gaussian_decomposition[:-1]:
        for index in range(len(gauss)):
            if ((highest_gauss[index] < gauss[index]) and (index > threshold) and (index < gaussian_mixture_model[-1].mean)):
                threshold = index
    return threshold, np.where(channel < threshold, 1, 0)