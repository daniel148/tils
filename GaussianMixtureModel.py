import numpy as np
import GMMWrapper as GMMWrapper
from scipy.stats import norm

def decompose_signal(channel, bins):
    counts, _ = np.histogram(channel, bins=bins)
    
    #GMM
    gaussian_mixture_model = GMMWrapper.decomposeGMM(list(range(0, bins)), counts, 0.0166, 10)
    
    gaussian_decomposition = []
    for gaussian_component in gaussian_mixture_model:
        x_axis = np.arange(0, 256)
        gaussian_decomposition.append(gaussian_component.weight*norm.pdf(x_axis, gaussian_component.mean,
                                  gaussian_component.deviation))
    return gaussian_decomposition;